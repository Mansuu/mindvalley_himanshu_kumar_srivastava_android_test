package test.android.himsnhu.mindvalley.imageloader.utils;

/**
 * Enum created for data type i.e. what kind of data to be received from server XML,JSON ,IMAGE etc.
 */
public class DataType {
  public static final int JSON=1;
    public static final int XML=2;
    public static final int IMAGE=3;

}
