package test.android.himsnhu.mindvalley.imageloader.notification;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import test.android.himsnhu.mindvalley.imageloader.utils.DataType;

/**
 * Created by himansh on 10/9/16.
 */
public class Notification {
    private static Notification notification;


    /**
     * private constructor to prevent instantiation of object
     */
    private Notification(){

    }

    /**
     *
     * @return only a single instance of Notification class as this class is singleton class
     */
    public static Notification getInstance(){
        if(notification==null){
            notification=new Notification();
        }
        return notification;
    }

    /**
     * notify client application,data has been downloaded
     */
    public synchronized void  sendNotification(int dataType, Context context, String data, Bitmap image, String key){
        Intent notificationIntent=new Intent();
        notificationIntent.putExtra(NotificationConstants.KEY,key);
        switch (dataType){
            case DataType.XML:
                case DataType.JSON:
                    notificationIntent.putExtra(NotificationConstants.DATA,data);
                    break;
                case DataType.IMAGE:
                    notificationIntent.putExtra(NotificationConstants.IMAGE,image);
                    break;
        }
        notificationIntent.putExtra(NotificationConstants.KEY,key);
        notificationIntent.putExtra(NotificationConstants.DATA_TYPE,dataType);
        notificationIntent.setAction(NotificationConstants.ACTION_DATA_DOWNLOAD);
        context.sendBroadcast(notificationIntent);
    }

    public synchronized void sendCancelNotification(Context context,String key){
        Intent notificationIntent=new Intent();
        notificationIntent.putExtra(NotificationConstants.KEY,key);
        notificationIntent.setAction(NotificationConstants.ACTION_CANCEL_DOWNLOAD);
        context.sendBroadcast(notificationIntent);
    }

}
