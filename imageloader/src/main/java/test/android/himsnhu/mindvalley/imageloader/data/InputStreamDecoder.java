package test.android.himsnhu.mindvalley.imageloader.data;

import android.graphics.Bitmap;

import java.io.InputStream;


/**
 * Created by himansh on 9/9/16.
 */
public abstract class InputStreamDecoder {
    public abstract String  decodeToString(InputStream i);
    public abstract Bitmap decodeToBitmap(InputStream i);
}
