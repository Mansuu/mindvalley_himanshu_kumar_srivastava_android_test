package test.android.himsnhu.mindvalley.imageloader.network;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * TO handle Network Connection
 */
public class NetworkManager {
    private HttpURLConnection httpURLConnectionObject;

    /*this method create Connection with server for each different url
     *
     * @param url
     * @return HttpUrlConnection object
     */
    public HttpURLConnection createAndOpenConnection(final String url){
        if(url!=null){
            try{

                URL finalUrl=new URL(url);
                httpURLConnectionObject=(HttpURLConnection) finalUrl.openConnection();
                httpURLConnectionObject.connect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


return httpURLConnectionObject;
    }

    /* close connection with server of a certain url

    *@param existing HttpUrlConnectionObject
    * @return
    *
     */
    public void closeConnection(HttpURLConnection oldHttpUrlConnectionObject){
        if(oldHttpUrlConnectionObject!=null){
            oldHttpUrlConnectionObject.disconnect();
        }
    }


}
