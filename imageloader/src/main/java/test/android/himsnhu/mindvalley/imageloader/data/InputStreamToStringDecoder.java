package test.android.himsnhu.mindvalley.imageloader.data;

import android.graphics.Bitmap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * extends abstract class InputStreamDecoder,this class is created to decode given
 * input stream received from server to String
 */
public class InputStreamToStringDecoder extends InputStreamDecoder {

    /**
     * decodes input stream to string
     * @param inputStream
     * @return string
     */
    @Override
    public String decodeToString(InputStream inputStream) {
        BufferedReader bufferedReader = null;
        StringBuilder outputStringBuilderObject = new StringBuilder();
        String line;

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = bufferedReader.readLine()) != null) {
                outputStringBuilderObject.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally{
            if(bufferedReader!=null){
                try {
                    bufferedReader.close();
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        return outputStringBuilderObject.toString();
    }


    /**
     * overriding not needed for this method
     * @param i
     * @return
     */
    @Override
    public Bitmap decodeToBitmap(InputStream i) {
        return null;
    }
}
