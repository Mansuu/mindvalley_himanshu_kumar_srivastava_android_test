package test.android.himsnhu.mindvalley.imageloader.notification;

/**
 * Created by himansh on 10/9/16.
 */
public class NotificationConstants{
    public static final String ACTION_DATA_DOWNLOAD="data download";
    public static  final String ACTION_CANCEL_DOWNLOAD="cancel download";
    public static final String DATA_TYPE="data type";
    public static final String DATA="data";
    public static final String IMAGE="image";
    public static final String KEY="key";
}
