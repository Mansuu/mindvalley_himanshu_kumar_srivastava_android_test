package test.android.himsnhu.mindvalley.imageloader.request;



import android.content.Context;
import android.util.Log;

import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;


public class RequestQueue {
    private static RequestQueue requestQueue ;
    private ConcurrentLinkedQueue<DownloadRequest> downloadRequestQueue=new ConcurrentLinkedQueue<DownloadRequest>();

    public static RequestQueue getInstance() {
        if(requestQueue==null){
            requestQueue=new RequestQueue();
        }
        return requestQueue;
    }

    private RequestQueue() {
    }

    /**
     * Adds new request to the request pool
     * @param conn
     * @param downloadUrl
     */
    public void enqueueDownloadRequest(HttpURLConnection conn, String downloadUrl, Context context,int dataType){
       DownloadRequest request=new DownloadRequest();
        request.setConneection(conn);
        request.setDownloadUrl(downloadUrl);
        Log.e("url",downloadUrl+"");
        request.setContext(context);
        request.setDataTYpe(dataType);
        downloadRequestQueue.add(request);
    }

    /**
     * removes request from request queue
     * @param downloadUrl
     */
    public void dequeueDownloadRequest(String downloadUrl){
      DownloadRequest request=getRequest(downloadUrl);
        if(request!=null){
            request.getConneection().disconnect();
        downloadRequestQueue.remove(request);

        }
    }

    /**
     *
     * @return concurrent queue which holds all requests
     */
    public ConcurrentLinkedQueue<DownloadRequest> getQueue(){
        return downloadRequestQueue;
    }


    /**
     *
     * @param url
     * @return request previously added in the request queue
     */
    public DownloadRequest getRequest(String url){
    Iterator<DownloadRequest> it=downloadRequestQueue.iterator();
    if(it.hasNext()){
        DownloadRequest request=it.next();
        String key=request.getDownloadUrl();
        if(key.equalsIgnoreCase(url)){
           return request;
        }

    }
    return  null;
}

}
