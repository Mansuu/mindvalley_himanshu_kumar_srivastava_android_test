package test.android.himsnhu.mindvalley.imageloader.request;

import android.content.Context;

import java.net.HttpURLConnection;

/**
 * Created by himansh on 11/9/16.
 */
public class DownloadRequest {
    private String downloadUrl;
    private HttpURLConnection conneection;
    private int dataTYpe;
    private Context context;

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public HttpURLConnection getConneection() {
        return conneection;
    }

    public void setConneection(HttpURLConnection conneection) {
        this.conneection = conneection;
    }

    public int getDataTYpe() {
        return dataTYpe;
    }

    public void setDataTYpe(int dataTYpe) {
        this.dataTYpe = dataTYpe;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
