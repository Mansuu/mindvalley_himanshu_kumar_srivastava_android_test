package test.android.himsnhu.mindvalley.imageloader.lrucache;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Singleton class Created for caching data
 */
public class LRUCache {
    private static LRUCache cache;

    final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);//Maximum memory availabe
    // Use 1/8th of the available memory for this memory cache.
    final int cacheSize = maxMemory / 8;

    //cache for caching bitmaps
    private LruCache<String, Bitmap> bitmapLruCache=new LruCache<String, Bitmap>(cacheSize){
        @Override
        protected int sizeOf(String key, Bitmap bitmap) {
            return super.sizeOf(key, bitmap);
        }
    };

   // cache for caching json or xml data
    private LruCache<String,String> stringLruCache=new LruCache<String, String>(cacheSize){
        @Override
        protected int sizeOf(String key, String stringData) {
            return super.sizeOf(key, stringData);
        }
    };

    //TODO private constructor as this class is singleton class
    private LRUCache(){

    }
    //TODO static method to return only one single instance of this class
    public static LRUCache getInstance(){
        if(cache==null){
            cache= new LRUCache();
        }
        return  cache;
    }

    /**
     * adds bitmap to lru cache
     * @param key contains url as key
     * @param value contains downloaded bitmap
     */
    public void addBitmapToCache(String key,Bitmap value) {
        if (getBitmapFromCache(key)==null) {
            bitmapLruCache.put(key,value);
        }
    }

    /**
     * adds json or xml string into cache
     * @param key contains url
     * @param value contains dowbloaded json or xml
     */
    public void addStringToCache(String key,String value){
    if(getStringFromCache(key)==null){
    stringLruCache.put(key,value);
}
    }

    /**
     *
     * @param key
     * @return cached bitmap
     */
    public Bitmap getBitmapFromCache(String key){
        return bitmapLruCache.get(key);
    }

    /**
     *
     * @param key
     * @return Cached json or xml String
     */
    public String getStringFromCache(String key){

        return stringLruCache.get(key);
}



}
