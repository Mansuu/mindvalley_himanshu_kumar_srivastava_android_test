package test.android.himsnhu.mindvalley.imageloader.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * created to download data from server using HttpUrlConnection class object
 */
public class DownloadManager {

    /**
     *
     * @param httpURLConnectionObject
     * @return input stream received from server
     */
    public InputStream download(HttpURLConnection httpURLConnectionObject){
        InputStream inputStream=null;
        try {
            int responseCode=httpURLConnectionObject.getResponseCode();
            if(responseCode==HttpURLConnection.HTTP_OK){
                inputStream=httpURLConnectionObject.getInputStream();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
return inputStream;
    }
}
