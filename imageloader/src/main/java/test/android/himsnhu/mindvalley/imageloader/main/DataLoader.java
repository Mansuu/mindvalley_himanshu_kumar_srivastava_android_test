package test.android.himsnhu.mindvalley.imageloader.main;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import test.android.himsnhu.mindvalley.imageloader.data.InputStreamDecoder;
import test.android.himsnhu.mindvalley.imageloader.data.InputStreamToBitmapDecoder;
import test.android.himsnhu.mindvalley.imageloader.data.InputStreamToStringDecoder;
import test.android.himsnhu.mindvalley.imageloader.lrucache.LRUCache;
import test.android.himsnhu.mindvalley.imageloader.network.DownloadManager;
import test.android.himsnhu.mindvalley.imageloader.network.NetworkManager;
import test.android.himsnhu.mindvalley.imageloader.notification.Notification;

import test.android.himsnhu.mindvalley.imageloader.request.DownloadRequest;
import test.android.himsnhu.mindvalley.imageloader.request.RequestQueue;
import test.android.himsnhu.mindvalley.imageloader.utils.DataType;


public class DataLoader {
    private NetworkManager networkManager;
    private DownloadManager downloadManager;
    private HttpURLConnection httpUrlConnectionObject;
   private LRUCache cache;
    private Notification notification;
private RequestQueue requestQueue=RequestQueue.getInstance();
    public static void getData(String url,int dataType,Context context){
        new DataLoader().startDownload(url,dataType,context);
    }

    /**
     *
     * @param url
     * @param dataType
     */
    private void startDownload(final String url, final int dataType, final Context context){
        //check cache whether this url exists n cache or not if not then go for downloading

            cache=LRUCache.getInstance();
        switch(dataType){
            case DataType.JSON:
            case DataType.XML:
               String cachedString= cache.getStringFromCache(url);
                if(cachedString!=null&&!cachedString.isEmpty()){
                    //NOtify app and return
                    getNotificationObject();
                    notification.sendNotification(dataType,context,cachedString,null,url);
                    return;
                }
                break;
            case DataType.IMAGE:
                Bitmap cachedBitmap=cache.getBitmapFromCache(url);
                if(cachedBitmap!=null){
                    getNotificationObject();
                    notification.sendNotification(dataType,context,null,cachedBitmap,url);
                    //NOtify app and return
                    return;
                }
                break;

        }
        networkManager=new NetworkManager();
        httpUrlConnectionObject=networkManager.createAndOpenConnection(url);
        addToQueue(httpUrlConnectionObject,context,dataType,url);
        downloadManager=new DownloadManager();
        InputStream receivedInputStream=downloadManager.download(httpUrlConnectionObject);
        InputStreamDecoder streamDecoder;
        switch(dataType){
            case DataType.XML:
                case DataType.JSON:
                    if(receivedInputStream!=null){
                        streamDecoder=new InputStreamToStringDecoder();
                        String outputString=streamDecoder.decodeToString(receivedInputStream);
                        if(outputString!=null){
                           closeInputStream(receivedInputStream);
                            //notify app
                            getNotificationObject();
                            notification.sendNotification(dataType,context,outputString,null,url);
                            requestQueue.dequeueDownloadRequest(url);
                            return;
                        }
                    }

                    break;
                case DataType.IMAGE:
                    if(receivedInputStream!=null){
                        streamDecoder=new InputStreamToBitmapDecoder();
                        Bitmap outputBitmap=streamDecoder.decodeToBitmap(receivedInputStream);
                        if(outputBitmap!=null){
                           closeInputStream(receivedInputStream);
                            //notify app
                            getNotificationObject();
                            notification.sendNotification(dataType,context,null,outputBitmap,url);
                            requestQueue.dequeueDownloadRequest(url);
                            return;
                        }
                    }
                    break;

        }

    }

    /***
     * cancels requested download
     * @param url
     */

    public static void cancelDownload(Context context,String url){
        //get connection object corresponding to the give url as key and disconnect
        Log.e("cancel download",url);
        RequestQueue requestQueue=RequestQueue.getInstance();
        DownloadRequest request=requestQueue.getRequest(url);
        if(request!=null){
            request.getConneection().disconnect();
            requestQueue.dequeueDownloadRequest(url);
            Notification.getInstance().sendCancelNotification(context,url);
        }

    }

    /**
     * close opened input stream
     * @param i
     */

    private void closeInputStream(InputStream i){
    if(i!=null){
        try {
            i.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}


    private void getNotificationObject(){
        notification= Notification.getInstance();
    }

    private void addToQueue(HttpURLConnection conn,Context context,int dataType,String url){
        requestQueue.enqueueDownloadRequest(conn,url,context,dataType);
    }

}
