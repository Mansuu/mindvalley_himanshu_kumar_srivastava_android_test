package test.android.himsnhu.mindvalley.imageloader.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;


/**
 * extends abstract class InputStreamDecoder, created to decode Input stream to bitmap
 */
public class InputStreamToBitmapDecoder extends InputStreamDecoder {


    @Override
    public String decodeToString(InputStream i) {
        return null;
    }


    /**
     *  decodes given input stream to bitmap
     * @param inputStream
     * @return bitmap
     */
    @Override
    public Bitmap decodeToBitmap(InputStream inputStream) {
        Bitmap outputBitmap=null;
        if(inputStream!=null){
            outputBitmap= BitmapFactory.decodeStream(inputStream);
        }
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputBitmap;
    }
}
