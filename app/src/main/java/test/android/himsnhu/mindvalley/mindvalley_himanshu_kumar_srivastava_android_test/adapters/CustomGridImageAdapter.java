package test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import test.android.himsnhu.mindvalley.imageloader.main.DataLoader;
import test.android.himsnhu.mindvalley.imageloader.utils.DataType;
import test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.R;
import test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.gettersetters.Data;


public class CustomGridImageAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<Data> dataList;
    private int layout_target;
    private LayoutInflater layoutInflater;
    private  ViewHolder holder;
    public CustomGridImageAdapter(Context context,int layout_target,ArrayList<Data> dataList){
        this.context=context;
        this.dataList=dataList;
        this.layout_target=layout_target;
        layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Data getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    /**
     * View holder class to contain views in grid item
     */
    private class ViewHolder{
    ImageView img_grid_source,img_download,img_cancel_download;
}
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
         holder=new ViewHolder();
        if(convertView==null){
            convertView=layoutInflater.inflate(layout_target,null);
            holder.img_grid_source=(ImageView)convertView.findViewById(R.id.img_grid_source);
            holder.img_download=(ImageView)convertView.findViewById(R.id.img_download);
            holder.img_cancel_download=(ImageView)convertView.findViewById(R.id.img_cancel_download);
            convertView.setTag(holder);
        }
        else{
            holder= (ViewHolder) convertView.getTag();
        }
       final Data dataObject=dataList.get(position);
        Bitmap defaultThumbnail= BitmapFactory.decodeResource(context.getResources(),
                R.drawable.thumbnail);
        if(defaultThumbnail!=null){
            holder.img_grid_source.setImageBitmap(defaultThumbnail);
            holder.img_download.setVisibility(View.VISIBLE);
            holder.img_cancel_download.setVisibility(View.GONE);
        }
        Bitmap thumbnail=dataObject.getThumbnail();
        if(thumbnail!=null){
            holder.img_grid_source.setImageBitmap(thumbnail);
            holder.img_download.setVisibility(View.GONE);
            holder.img_cancel_download.setVisibility(View.GONE);
        }
        holder.img_download
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.img_download.setVisibility(View.GONE);
                        holder.img_cancel_download.setVisibility(View.VISIBLE);
                        download(dataObject.getImageUrl());
                    }
                });
        holder.img_cancel_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cancel download
                DataLoader.cancelDownload(context,dataObject.getImageUrl());
            }
        });
        return convertView;
    }

    /**
     *
     * @param key
     * @param thumbnail
     */
    public void setDownloadedImage(String key, Bitmap thumbnail){
        for(int position=0;position<getCount();position++){
            Data dataObject=dataList.get(position);
            if(dataObject.getImageUrl().equalsIgnoreCase(key)){
                dataObject.setThumbnail(thumbnail);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void cancelImageDownloading(String key){
        for(int position=0;position<getCount();position++){
            Data dataObject=dataList.get(position);
            if(dataObject.getImageUrl().equalsIgnoreCase(key)){
               dataObject.setThumbnail(null);
                break;
            }
        }
        notifyDataSetChanged();
    }

    /**
     *
     * @param url
     */
    private void download(final String url){
        new AsyncTask<Void,Void,Void>(){

            @Override
            protected Void doInBackground(Void... params) {
                DataLoader.getData(url, DataType.IMAGE,context);
                return null;
            }
        }.execute();
    }


}
