package test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.GridView;

import java.util.ArrayDeque;
import java.util.ArrayList;

import test.android.himsnhu.mindvalley.imageloader.main.DataLoader;
import test.android.himsnhu.mindvalley.imageloader.notification.NotificationConstants;
import test.android.himsnhu.mindvalley.imageloader.utils.DataType;
import test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.R;
import test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.adapters.CustomGridImageAdapter;
import test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.downloadeddata.JSONParser;
import test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.gettersetters.Data;
import test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.network.CheckNetworkState;
import test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.utils.GlobalConstants;

public class MainActivity extends AppCompatActivity {
    private Context context;
    private GridView grid_image;
    private BroadcastReceiver dataDownloadReceiver;
    private CoordinatorLayout coordinator_layout;
    private CustomGridImageAdapter gridViewAdapter;
    private JSONParser jsonParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        if(CheckNetworkState.isNetworkAvailable(context)){
            download();
        }
        else{
            Snackbar.make(coordinator_layout,getString(R.string.internet_not_available),Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        dataDownloadReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action=intent.getAction();
                if(action.equalsIgnoreCase(NotificationConstants.ACTION_DATA_DOWNLOAD)){
                    int dataType=intent.getIntExtra(NotificationConstants.DATA_TYPE,DataType.JSON);
                    switch(dataType){
                        case DataType.JSON:
                        case DataType.XML:
                            String data=intent.getStringExtra(NotificationConstants.DATA);
                            Log.e("downloaded data",data+"");
                            jsonParser =new JSONParser();
                            ArrayList<Data> dataList=jsonParser.parse(data);
                            gridViewAdapter=new CustomGridImageAdapter(context,R.layout.layout_grid_item,dataList);
                            grid_image.setAdapter(gridViewAdapter);
                            break;
                        case DataType.IMAGE:
                            String key=intent.getStringExtra(NotificationConstants.KEY);
                            Bitmap bmp=intent.getParcelableExtra(NotificationConstants.IMAGE);
                            if(gridViewAdapter!=null){
                                //set downloaded image in appropriate grid position
                               gridViewAdapter.setDownloadedImage(key,bmp);

                            }
                            break;
                    }
                }
                else if(action.equalsIgnoreCase(NotificationConstants.ACTION_CANCEL_DOWNLOAD)){
                    String key=intent.getStringExtra(NotificationConstants.KEY);
                    if(gridViewAdapter!=null){
                        gridViewAdapter.cancelImageDownloading(key);

                    }
                }

            }
        };
        IntentFilter filter=new IntentFilter();
        filter.addAction(NotificationConstants.ACTION_DATA_DOWNLOAD);
        IntentFilter filterCancelDownload=new IntentFilter();
        filterCancelDownload.addAction(NotificationConstants.ACTION_CANCEL_DOWNLOAD);
        registerReceiver(dataDownloadReceiver,filter);
        registerReceiver(dataDownloadReceiver,filterCancelDownload);
    }

    /**
     * initalize variables
     */
    private void init(){
        context=MainActivity.this;
        grid_image=(GridView)findViewById(R.id.grid_image);
        coordinator_layout=(CoordinatorLayout)findViewById(R.id.layout_main);
    }

    private void download(){
        new AsyncTask<Void,ArrayList<Data>,ArrayList<Data>>(){

            @Override
            protected ArrayList<Data> doInBackground(Void... params) {
                DataLoader.getData(GlobalConstants.UNIVERSAL_DEFAULT_URL,DataType.JSON,context);
                return null;
            }


        }.execute();

    }
    
}
