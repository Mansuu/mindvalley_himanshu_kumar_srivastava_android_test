package test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.downloadeddata;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.gettersetters.Data;

public class JSONParser {
    public ArrayList<Data> parse(String jsonString){
        ArrayList<Data> dataItems=null;
        if(jsonString!=null){
            try{
                JSONArray array=new JSONArray(jsonString);
                if(array!=null){
                    dataItems=new ArrayList<Data>();
                    for(int i=0;i<array.length();i++){
                        JSONObject jsonObject=array.getJSONObject(i);
                        if(jsonObject!=null){
                            Data data=new Data();
                            String id=jsonObject.getString("id");
                            JSONObject ob=jsonObject.getJSONObject("urls");
                            String imageUrl=ob.getString("thumb");
                            data.setId(id);
                            data.setImageUrl(imageUrl);
                            dataItems.add(data);

                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return dataItems;
    }
}
