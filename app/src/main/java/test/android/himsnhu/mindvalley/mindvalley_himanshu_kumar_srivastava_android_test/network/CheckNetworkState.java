package test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class CheckNetworkState {

	/**
	 * @param mContext
	 * @return internet is available or not
	 */
	public static boolean isNetworkAvailable(Context mContext) {
		boolean outcome = false;
		if (mContext != null) {
			ConnectivityManager cm = (ConnectivityManager) mContext
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo=cm.getActiveNetworkInfo();
			if(networkInfo!=null){
				if(networkInfo.isConnected()){
					outcome=true;
				}
			}

		}
		return outcome;
	}



}
