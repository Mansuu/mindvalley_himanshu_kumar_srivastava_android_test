package test.android.himsnhu.mindvalley.mindvalley_himanshu_kumar_srivastava_android_test.gettersetters;

import android.graphics.Bitmap;

public class Data {
    private String id,imageUrl;
    private Bitmap thumbnail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }
}
